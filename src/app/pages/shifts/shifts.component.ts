import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Shift } from '../../models/shift';
import { ShiftService } from '../../service/shift.service';
import { Day } from '../../utils/day';

@Component({
  selector: 'app-shifts',
  templateUrl: './shifts.component.html',
  styleUrls: ['./shifts.component.less']
})
export class ShiftsComponent implements OnInit {

  readyForm = false;

  shifts: Shift[];
  displayDialog: boolean;
  shift: Shift = new Shift();
  selectedShift: Shift;
  newShift: boolean;

  constructor(private shiftService: ShiftService) { }

  ngOnInit(): void {
    this.shiftService.shiftsSubject.subscribe(s => {
      this.shifts = s ? s : [];
    });

    this.shiftService.getAllShifts();
  }

  showDialogToAdd(): void {
    this.newShift = true;
    this.shift = new Shift();
    this.displayDialog = true;
  }

  save(): void {
    this.shiftService.saveShift(this.shift).subscribe(newShift => {
      this.shifts.push(newShift);
      this.shift = new Shift();
      this.displayDialog = false;
    });
  }

  update(): void {
    this.shiftService.updateShift(this.shift).subscribe(shift => {
      const itemIndex = this.shifts.findIndex(s => s.id === shift.id);
      this.shifts[itemIndex] = this.shift;
      this.displayDialog = false;
    });
  }

  delete(): void {
    this.shiftService.deleteShift(this.shift).subscribe(shift => {
      const index = this.shifts.indexOf(this.selectedShift);
      this.shifts = this.shifts.filter(s => s.id !== this.shift.id);
      this.shift = new Shift();
      this.displayDialog = false;
    });
  }

  onRowSelect(event): void {
    this.newShift = false;
    this.shift = this.cloneShift(event.data);
    this.displayDialog = true;
  }

  cloneShift(c: Shift): Shift {
    const s: Shift = new Shift();

    s.id = c.id;
    s.day = c.day;

    return s;
  }

  checkDay(e): void {
    if (!Object.values(Day).includes(e.value)) {
      this.shift.day = this.shift.day.filter(a => a.valueOf() !== e.value.valueOf());
    } else if (this.shift.day.length > 1) {
      this.shift.day = this.shift.day.filter(a => a.valueOf() !== e.value.valueOf());
    }
  }

  get disabledForm(): boolean {
    return (this.shift ? this.shift.day.length : 0) > 0;
  }

}
