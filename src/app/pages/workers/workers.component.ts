import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { WorkerService } from '../../service/worker.service';
import { Worker } from '../../models/worker';
import { Day } from '../../utils/day';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.less']
})
export class WorkersComponent implements OnInit {

  readyForm = false;

  workers: Worker[];
  displayDialog: boolean;
  worker: Worker = new Worker();
  selectedWorker: Worker;
  newWorker: boolean;

  constructor(private workerService: WorkerService) { }

  ngOnInit(): void {
    this.workerService.workersSubject.subscribe(w => {
      this.workers = w ? w : [];
    });

    this.workerService.getAllWorkers();
  }

  showDialogToAdd(): void {
    this.newWorker = true;
    this.worker = new Worker();
    this.displayDialog = true;
  }

  save(): void {
    this.workerService.saveWorker(this.worker).subscribe(newWorker => {
      this.workers.push(newWorker);
    });

    this.worker = new Worker();
    this.displayDialog = false;
  }

  update(): void {
    this.workerService.updateWorker(this.worker).subscribe(worker => {
      const itemIndex = this.workers.findIndex(w => w.id === worker.id);
      this.workers[itemIndex] = this.worker;
      this.displayDialog = false;
    });
  }

  delete(): void {
    this.workerService.deleteWorker(this.worker).subscribe(worker => {
      this.workers = this.workers.filter(w => w.id !== this.worker.id);
      this.worker = new Worker();
      this.displayDialog = false;
    });
  }

  onRowSelect(event): void {
    this.newWorker = false;
    this.worker = this.cloneWorker(event.data);
    this.displayDialog = true;
  }

  cloneWorker(c: Worker): Worker {
    const w: Worker = new Worker();

    w.id = c.id;
    w.availability = c.availability;
    w.payrate = c.payrate;

    return w;
  }

  checkAvailability(e): void {
    if (!Object.values(Day).includes(e.value)) {
      this.worker.availability = this.worker.availability.filter(a => a.valueOf() !== e.value.valueOf());
    }
  }

  get disabledForm(): boolean {
    return (this.worker ? this.worker.availability.length : 0) > 0
      && !!Number.parseFloat(this.worker.payrate ? this.worker.payrate.toString() : '0');
  }

  get totalCost(): number {
    let total = 0;
    this.workers.forEach(w => total += w.payrate);

    return total;
  }
}
