import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Worker } from '../../models/worker';
import { WorkerService } from '../../service/worker.service';
import { Day } from '../../utils/day';
import { Shift } from '../../models/shift';
import { ShiftService } from '../../service/shift.service';
import { MatchingService } from '../../service/matching.service';
import { OutputMappings } from '../../utils/outputMappings';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

  shifts: Shift[];
  workers: Worker[];
  mappings: OutputMappings;

  constructor(
    private workerService: WorkerService,
    private shiftService: ShiftService,
    private matchingService: MatchingService) {
  }

  ngOnInit(): void {
    this.workerService.workersSubject.subscribe(w => {
      this.workers = w ? w : [];
    });
    this.shiftService.shiftsSubject.subscribe(s => {
      this.shifts = s ? s : [];
    });
    this.matchingService.matchingsSubject.subscribe(m => {
      this.mappings = m ? m : new OutputMappings();
    });

    this.workerService.getAllWorkers();
    this.shiftService.getAllShifts();
    this.matchingService.getAllMatchings();
  }

  get totalCost(): number {
    return this.mappings ? this.mappings.totalCost : 0;
  }

}
