import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Matching } from '../models/matching';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { OutputMappings } from '../utils/outputMappings';

@Injectable({
  providedIn: 'root'
})
export class MatchingService {

  matchingsSubject: BehaviorSubject<OutputMappings> = new BehaviorSubject(undefined);
  private readonly matchingsApi = '/matchings/';

  constructor(private _http: HttpClient) { }

  getAllMatchings(): void {
    this._http.get<OutputMappings>(environment.backendUrl + this.matchingsApi).subscribe(m => {
      this.matchingsSubject.next(m);
    });
  }
}
