import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Shift } from '../models/shift';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShiftService {

  shiftsSubject: BehaviorSubject<Shift[]> = new BehaviorSubject(undefined);
  private readonly shiftsApi = '/shifts/';

  constructor(private _http: HttpClient) {
  }

  getAllShifts(): void {
    this._http.get<Shift[]>(environment.backendUrl + this.shiftsApi).subscribe(shifts => {
      this.shiftsSubject.next(shifts);
    });
  }

  saveShift(shift: Shift): Observable<Shift> {
    return this._http.post<Shift>(environment.backendUrl + this.shiftsApi, shift);
  }

  updateShift(shift: Shift): Observable<Shift> {
    return this._http.put<Shift>(environment.backendUrl + this.shiftsApi + shift.id, shift);
  }

  deleteShift(shift: Shift): Observable<Shift> {
    return this._http.delete<Shift>(environment.backendUrl + this.shiftsApi + shift.id);
  }
}
