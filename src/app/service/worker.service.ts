import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Worker } from '../models/worker';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkerService {

  workersSubject: BehaviorSubject<Worker[]> = new BehaviorSubject(undefined);
  private readonly workersApi = '/workers/';

  constructor(private _http: HttpClient) {
  }

  getAllWorkers(): void {
    this._http.get<Worker[]>(environment.backendUrl + this.workersApi).subscribe(workers => {
      this.workersSubject.next(workers);
    });
  }

  saveWorker(worker: Worker): Observable<Worker> {
    return this._http.post<Worker>(environment.backendUrl + this.workersApi, worker);
  }

  updateWorker(worker: Worker): Observable<Worker> {
    return this._http.put<Worker>(environment.backendUrl + this.workersApi + worker.id, worker);
  }

  deleteWorker(worker: Worker): Observable<Worker> {
    return this._http.delete<Worker>(environment.backendUrl + this.workersApi + worker.id);
  }
}
