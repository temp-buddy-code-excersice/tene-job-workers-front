import { Worker } from '../models/worker';
import { Shift } from '../models/shift';

export class InputLists {
  workers: Worker[];
  shifts: Shift[];
}
