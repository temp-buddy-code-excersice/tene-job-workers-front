import { Matching } from '../models/matching';

export class OutputMappings {
  mappings: Matching[];
  totalCost: number;

  constructor() {
    this.mappings = [];
    this.totalCost = 0.00;
  }
}
