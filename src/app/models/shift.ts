import { Day } from '../utils/day';

export class Shift {
  id: number;
  day: Day[];

  constructor() {
    this.id = undefined;
    this.day = [];
  }
}
