import { Shift } from './shift';
import { Worker } from './worker';

export class Matching {
  id: number;
  worker: Worker;
  shift: Shift;
}
