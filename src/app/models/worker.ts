import { Day } from '../utils/day';

export class Worker {
  id: number;
  availability: Day[];
  payrate: number;

  constructor() {
    this.id = undefined;
    this.availability = [];
    this.payrate = undefined;
  }
}
