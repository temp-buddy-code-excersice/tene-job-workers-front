import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { environment } from '../../../environments/environment';
import { MenuItem } from 'primeng/components/common/menuitem';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {

  items: MenuItem[];
  swaggerUrl = `${environment.backendUrl}/swagger-ui.html`;

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Dashboard',
        icon: 'fa fa-home',
        command: c => this.router.navigate(['dashboard'])
      },
      {
        label: 'Workers',
        icon: 'fa fa-wrench',
        command: c => this.router.navigate(['workers'])
      },
      {
        label: 'Shifts',
        icon: 'fa fa-sticky-note',
        command: c => this.router.navigate(['shifts'])
      },
      {
        label: 'Api documentation',
        icon: 'pi pi-external-link',
        command: c => window.open(this.swaggerUrl)
      }
    ];
  }

}
