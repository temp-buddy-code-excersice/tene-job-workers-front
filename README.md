# TeneJobWorkersFront

<img src="https://img.shields.io/badge/angular--cli-6.1.1-green.svg">
&nbsp;
<img src="https://img.shields.io/badge/angular-6.1.0-green.svg">
&nbsp;
<img src="https://img.shields.io/badge/node-8.11.3-green.svg">
&nbsp;


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.1.

## Development server

Run `npm run start:dev|docker` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. ( dev or docker environments will depends on where you have your API, docker: http://192.168.99.100:2030, or local http://localhost:2030 )

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build:prod` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
