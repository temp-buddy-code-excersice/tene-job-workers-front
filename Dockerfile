FROM nginx:1.15-alpine-perl

RUN apk add --no-cache git mercurial
RUN apk add --update nodejs nodejs-npm \
  && npm i npm@latest -g
RUN mkdir -p /home/

WORKDIR /home
RUN git clone https://gitlab.com/temp-buddy-code-excersice/tene-job-workers-front.git

WORKDIR /home/tene-job-workers-front

RUN npm install
RUN npm run build:prod
RUN cp -R /home/tene-job-workers-front/dist/tene-job-workers-front/* /usr/share/nginx/html

WORKDIR /home
RUN rm -rf /home/tene-job-workers-front

COPY default.conf /etc/nginx/conf.d/
